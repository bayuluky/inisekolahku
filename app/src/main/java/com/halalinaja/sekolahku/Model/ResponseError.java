package com.halalinaja.sekolahku.Model;

/**
 * Created by Zonth on 23-May-17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseError {
    @SerializedName("status")   @Expose private String status;
    @SerializedName("count")    @Expose private String count;
    @SerializedName("message")  @Expose private String message;

    public String getStatus() {
        return status;
    }

    public String getCount() {
        return count;
    }

    public String getMessage() {
        return message;
    }
}
