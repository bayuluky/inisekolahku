package com.halalinaja.sekolahku.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zonth on 23-May-17.
 */

public class ResponseUser {
    @SerializedName("status")   @Expose
    private String status;
    @SerializedName("count")    @Expose private String count;
    @SerializedName("message")  @Expose private String message;
    @SerializedName("data")     @Expose private List<User> UserData;

    public String getStatus() {
        return status;
    }

    public String getCount() {
        return count;
    }

    public String getMessage() {
        return message;
    }

    public List<User> getUserData() {
        return UserData;
    }

    public void setUserData(ArrayList<User> UserData) {
        this.UserData = UserData;
    }
}
