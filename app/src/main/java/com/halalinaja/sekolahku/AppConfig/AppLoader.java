package com.halalinaja.sekolahku.AppConfig;

/**
 * Created by Zonth on 23-May-17.
 */

import android.app.Application;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

public class AppLoader extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Don't do this! This is just so cold launches take some time
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
    }
}
