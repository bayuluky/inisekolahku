package com.halalinaja.sekolahku.AppConfig;

/**
 * Created by Zonth on 23-May-17.
 */

import  com.halalinaja.sekolahku.Model.ResponseUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AppInterface {

    interface LoginInterface {
        @FormUrlEncoded
        @POST("user/login")
        Call<ResponseUser> LoginUser (
                @Field("email") String email,
                @Field("password") String pass
        );
    }

    interface RegisterInterface {
        @FormUrlEncoded
        @POST("user/register")
        Call<ResponseUser> RegisterUser (
                @Field("no_hp") String no_hp,
                @Field("name") String name,
                @Field("email") String email,
                @Field("password") String pass,
                @Field("log_as") int log_as,
                @Field("grade") int grade
        );
    }
}