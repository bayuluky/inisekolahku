package com.halalinaja.sekolahku.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.halalinaja.sekolahku.AppConfig.AppSessionManager;
import com.halalinaja.sekolahku.R;

public class SplashActivity extends AppCompatActivity {
    AppSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Session Manager
        session = new AppSessionManager(getApplicationContext());

        if(!session.isLoggedIn()){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            } else {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
        }

        finish();
    }
}
