package com.halalinaja.sekolahku.View;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.halalinaja.sekolahku.AppConfig.AppInterface;
import com.halalinaja.sekolahku.Model.ResponseError;
import com.halalinaja.sekolahku.Model.ResponseUser;
import com.halalinaja.sekolahku.Model.User;
import com.halalinaja.sekolahku.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.halalinaja.sekolahku.AppConfig.AppGlobalVariable.APP_BASE_URL;

public class RegisterActivity extends Activity{

    private EditText mMobileNumberView, mFullNameView, mEmailView, mPasswordView, mConfirmPasswordView;
    private View mProgressView;
    private View mRegisterFormView;
    private View mLayoutRegisterButtonView;
    private Spinner spinner;

    int logAs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mMobileNumberView = (EditText) findViewById(R.id.mobileNumber);
        mFullNameView = (EditText) findViewById(R.id.fullName);
        mEmailView = (EditText) findViewById(R.id.userEmailId);
        mPasswordView = (EditText) findViewById(R.id.password);
        mConfirmPasswordView = (EditText) findViewById(R.id.confirmPassword);

        spinner = (Spinner) findViewById(R.id.account_type);

        spinner.getBackground().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.register_type_choice_array, R.layout.spinner_account_type_register);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_account_type_register);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        // Register
        Button mRegisterButton = (Button) findViewById(R.id.signUpBtn);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        // Go to Login
        TextView mLoginButton = (TextView) findViewById(R.id.alreadyUser);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goActivityLogin();
            }
        });

        mRegisterFormView = findViewById(R.id.register_form);
        mProgressView = findViewById(R.id.login_progress);
        mLayoutRegisterButtonView = findViewById(R.id.layoutRegisterButton);
    }

    private void attemptRegister() {

        // Reset errors.
        mFullNameView.setError(null);
        mEmailView.setError(null);
        mMobileNumberView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);

        // Store values at the time of the register attempt.
        String fullname = mFullNameView.getText().toString();
        String email = mEmailView.getText().toString();
        String mobileNumber = mMobileNumberView.getText().toString();
        String password = mPasswordView.getText().toString();
        String passwordConfirm = mConfirmPasswordView.getText().toString();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if( position == 0 )
                    logAs = 2;
                else if( position == 1 )
                    logAs = 1;

//                Toast.makeText
//                        (getApplicationContext(), "Selected : " + logAs, Toast.LENGTH_SHORT)
//                        .show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                logAs = 2;
            }
        });

        boolean cancel = false;
        View focusView = null;

        // Check for a valid full name address.
        if (TextUtils.isEmpty(fullname)) {
            mFullNameView.setError(getString(R.string.error_field_required));
            focusView = mFullNameView;
            cancel = true;
        }
        // Check for a valid email, if the user entered one.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        // Check for a valid mobile number, if the user entered one.
        if (TextUtils.isEmpty(mobileNumber)) {
            mMobileNumberView.setError(getString(R.string.error_field_required));
            focusView = mMobileNumberView;
            cancel = true;
        }
        // Check for a valid mobile number, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
        // Check for a valid mobile number, if the user entered one.
        if (TextUtils.isEmpty(passwordConfirm)) {
            mConfirmPasswordView.setError(getString(R.string.error_field_required));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        if (password.length() < 6) {
            mPasswordView.setError("Password terdiri dari lebih dari 6 karakter.");
            focusView = mPasswordView;
            cancel = true;
        }

        if (!password.matches( passwordConfirm )) {
            mPasswordView.setError("Pastikan password anda sama.");
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            User userData = new User();
            userData.setName(fullname);
            userData.setEmail(email);
            userData.setMobileNumber(mobileNumber);
            userData.setPassword(password);
            userData.setLogAs(logAs);

            sendRegisterRequest(userData);
        }
    }

    private void sendRegisterRequest(User userData) {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(APP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        final Retrofit retrofit = builder.build();

        AppInterface.RegisterInterface ReqRegister = retrofit.create(AppInterface.RegisterInterface.class);
        // if log_as = 1 then grade = null
        // if log_as = 2 then grade = 0
        //Call<ResponseUser> call = ReqRegister.RegisterUser(mobileNumber, fullname, email, password, email , 2, 0);
        Call<ResponseUser> call = ReqRegister.RegisterUser(userData.getMobileNumber(), userData.getName(), userData.getEmail(), userData.getPassword(), userData.getLogAs(), (userData.getLogAs() == 1) ? null: 0);
        call.enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                showProgress(false);
                if (response.isSuccessful()) {
                    ResponseUser mUser = response.body();
                    //session.createLoginSession(mUserData.get(i).getUsername(), mUserData.get(i).getMkNama());
                    Snackbar.make(mRegisterFormView, mUser.getMessage(), Snackbar.LENGTH_LONG).show();
                    //Snackbar.make(, mUser.getMessage(), Snackbar.LENGTH_SHORT).show();
                    //Toast.makeText(RegisterActivity.this, mUser.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else if (!response.isSuccessful()) {
                    Gson gson       = new GsonBuilder().create();
                    ResponseError mError = new ResponseError();
                    try {
                        mError=gson.fromJson(response.errorBody().string(), ResponseError.class);
                        Snackbar.make(mRegisterFormView, mError.getMessage(), Snackbar.LENGTH_LONG).show();
                        //Toast.makeText(RegisterActivity.this, mError.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Snackbar.make(mRegisterFormView, e.getMessage(), Snackbar.LENGTH_LONG).show();
                        //Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {
                showProgress(false);
                Snackbar.make(mRegisterFormView, "Ups, Something Wrong. Please check connection or any problem in your device", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void goActivityLogin() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLayoutRegisterButtonView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLayoutRegisterButtonView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLayoutRegisterButtonView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLayoutRegisterButtonView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}
