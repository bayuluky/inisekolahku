package com.halalinaja.sekolahku.View;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.halalinaja.sekolahku.AppConfig.AppInterface;
import com.halalinaja.sekolahku.AppConfig.AppSessionManager;
import com.halalinaja.sekolahku.Model.ResponseError;
import com.halalinaja.sekolahku.Model.ResponseUser;
import com.halalinaja.sekolahku.Model.User;
import com.halalinaja.sekolahku.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.halalinaja.sekolahku.AppConfig.AppGlobalVariable.APP_BASE_URL;

public class LoginActivity extends AppCompatActivity {
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private View mLayoutLoginButtonView;
    private AppSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Session Manager
        session = new AppSessionManager(getApplicationContext());

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
            }
        });

        Button mUsernameSignInButton = (Button) findViewById(R.id.loginBtn);
        mUsernameSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        // Register for new account
        TextView mCreateAccountButton = (TextView) findViewById(R.id.createAccount);
        mCreateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goActivityRegister();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mLayoutLoginButtonView = findViewById(R.id.layoutLoginButton);
    }

    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;

            cancel = true;
        }
        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            sendLoginRequest(email, password);
        }
    }

    private void sendLoginRequest(String email, String password) {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(APP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        final Retrofit retrofit = builder.build();

        AppInterface.LoginInterface ReqLogin = retrofit.create(AppInterface.LoginInterface.class);
        Call<ResponseUser> call = ReqLogin.LoginUser(email,password);
        call.enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                showProgress(false);
                if (response.isSuccessful()) {
                    ResponseUser mUser = response.body();
                    List<User> mUserData = response.body().getUserData();
                    for(int i = 0; i < mUserData.size(); i++) {
                        session.createLoginSession(mUserData.get(i).getEmail(), mUserData.get(i).getName());
                        Snackbar.make(mLoginFormView, mUser.getMessage(), Snackbar.LENGTH_SHORT).show();
                        //Toast.makeText(LoginActivity.this, mUser.getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else if (!response.isSuccessful()) {
                    Gson gson       = new GsonBuilder().create();
                    ResponseError mError = new ResponseError();
                    try {
                        mError=gson.fromJson(response.errorBody().string(),ResponseError.class);
                        Snackbar.make(mLoginFormView, mError.getMessage(), Snackbar.LENGTH_LONG).show();
                        //Toast.makeText(LoginActivity.this, mError.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Snackbar.make(mLoginFormView, mError.getMessage(), Snackbar.LENGTH_LONG).show();
                        //Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {
                showProgress(false);
                Snackbar.make(mLoginFormView, "Ups, Something Wrong. Please check connection or any problem in your device", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void goActivityRegister() {

        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        //finish();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLayoutLoginButtonView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLayoutLoginButtonView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLayoutLoginButtonView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLayoutLoginButtonView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
