package com.halalinaja.sekolahku.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.halalinaja.sekolahku.AppConfig.AppSessionManager;
import com.halalinaja.sekolahku.R;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private TextView tvUsername;
    private String account_username, account_name;
    AppSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Session Login
        session = new AppSessionManager(getApplicationContext());
        HashMap<String, String> account = session.getUserDetails();
        account_username = account.get(AppSessionManager.KEY_USERNAME);
        account_name = account.get(AppSessionManager.KEY_NAME);

        tvUsername = (TextView) findViewById(R.id.tvUsername);

        tvUsername.setText(account_username + " (" + account_name + ")");
    }

    @Override
    protected void onPause() {
        super.onPause();

        session = new AppSessionManager(getApplicationContext());
        session.logoutUser();
    }
}
